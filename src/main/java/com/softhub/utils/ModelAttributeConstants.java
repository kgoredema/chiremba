package com.softhub.utils;

public enum ModelAttributeConstants {

    MESSAGE("message");

    private String name;

    ModelAttributeConstants(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
