package com.softhub.utils;

public enum MessageType {

    INFO, SUCCESS, WARNING, ERROR

}
