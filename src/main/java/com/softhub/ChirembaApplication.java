package com.softhub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import com.github.ulisesbocchio.jar.resources.JarResourceLoader;

@SpringBootApplication
@EntityScan(basePackages = {"com.softhub", "com.econetwireless.common.scheduledtask.domain"})
public class ChirembaApplication {

	public static void main(String[] args) {

		new SpringApplicationBuilder()
				.sources(ChirembaApplication.class)
				.resourceLoader(new JarResourceLoader())
				.build()
				.run(args);

	}

}
