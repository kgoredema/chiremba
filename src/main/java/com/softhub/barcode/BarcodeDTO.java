package com.softhub.barcode;

import com.softhub.chiremba.business.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by kelvin on 2019/06/13.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BarcodeDTO {

    private Long barcode;
    private Product product;
    private String unit;
    private BigDecimal unitSize;
    protected LocalDateTime created;
    protected LocalDateTime lastModified;
    protected String createdBy;
    protected String modifiedBy;
    private int version;
    private Long id;
}
