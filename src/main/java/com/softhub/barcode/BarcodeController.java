package com.softhub.barcode;

import com.softhub.chiremba.business.barcode.Barcode;
import com.softhub.chiremba.business.barcode.BarcodeService;
import com.softhub.chiremba.business.exception.ChirembaException;
import com.softhub.chiremba.business.product.Product;
import com.softhub.chiremba.business.product.ProductService;
import com.softhub.product.ProductDTO;
import com.softhub.utils.Message;
import com.softhub.utils.MessageType;
import com.softhub.utils.ModelAttributeConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by kelvin on 2019/06/13.
 */
@Controller
@RequestMapping("/barcode")
@RequiredArgsConstructor
@Slf4j
public class BarcodeController {

    private final BarcodeService service;
    private final ProductService productService;

    @GetMapping("/edit")
    public String form(@RequestParam(value = "id", required = false) Long id, Model model) {
        BarcodeDTO barcodeDTO = null;
        if (id != null) {
            final Optional<Barcode> optionalBarcode = service.get(id);
            if (optionalBarcode.isPresent()) {
                barcodeDTO = createBarcodeDTO(optionalBarcode.get());
            }
        } else {
            barcodeDTO = new BarcodeDTO();
        }
        model.addAttribute("barcode", barcodeDTO);
        model.addAttribute("title", "Add Barcode");
        model.addAttribute("products",productService.getAll());


        return "barcode/edit";
    }

    @PostMapping("/edit")
    public String processForm(@Valid BarcodeDTO barcodeDTO, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message("Validation  Errors :", MessageType.ERROR));
            model.addAttribute("barcode", barcodeDTO);
            return "barcode/edit";
        }
        try {
            service.save(createBarcode(barcodeDTO));
            redirectAttributes.addFlashAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message("Cashier saved successfully!", MessageType.INFO));
        } catch (ChirembaException e) {
            log.error("Unable to save  cashier", e);
            redirectAttributes.addFlashAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message(e.getMessage(), MessageType.ERROR));
        }
        return "redirect:/barcode/list/";

    }
    @GetMapping("/list")
    public String productList(Model model) {
        final List<BarcodeDTO>  barcodes = service.getAll().stream().map(this::createBarcodeDTO).collect(Collectors.toList());
        model.addAttribute("list", barcodes);
        return "barcode/list";
    }

    private BarcodeDTO createBarcodeDTO(Barcode barcode) {
        return BarcodeDTO.builder().id(barcode.getId())
                .barcode(barcode.getBarcode())
                .unit(barcode.getUnit())
                .unitSize(barcode.getUnitSize())
                .product(barcode.getProduct())
                .created(barcode.getCreated())
                .version(barcode.getVersion())
                .build();
    }

    private Barcode createBarcode(BarcodeDTO barcodeDTO) {
        Barcode barcode = new Barcode();
        barcode.setProduct(barcodeDTO.getProduct());
        barcode.setBarcode(barcodeDTO.getBarcode());
        barcode.setVersion(barcodeDTO.getVersion());
        barcode.setCreated(barcodeDTO.getCreated());
        barcode.setCreatedBy(barcodeDTO.getCreatedBy());
        barcode.setModifiedBy(barcodeDTO.getModifiedBy());
        barcode.setUnit(barcodeDTO.getUnit());
        barcode.setUnitSize(barcodeDTO.getUnitSize());
        return barcode;
    }



}
