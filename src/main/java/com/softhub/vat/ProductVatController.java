package com.softhub.vat;

import com.softhub.barcode.BarcodeDTO;
import com.softhub.chiremba.business.barcode.Barcode;
import com.softhub.chiremba.business.barcode.BarcodeService;
import com.softhub.chiremba.business.exception.ChirembaException;
import com.softhub.chiremba.business.product.ProductService;
import com.softhub.chiremba.business.vat.ProductVat;
import com.softhub.chiremba.business.vat.ProductVatService;
import com.softhub.utils.Message;
import com.softhub.utils.MessageType;
import com.softhub.utils.ModelAttributeConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by kelvin on 2019/06/13.
 */

@Controller
@RequestMapping("/product-vat")
@RequiredArgsConstructor
@Slf4j
public class ProductVatController {

    private final ProductVatService service;

    @GetMapping("/edit")
    public String form(@RequestParam(value = "id", required = false) Long id, Model model) {
        ProductVatDTO productVatDTO = null;
        if (id != null) {
            final Optional<ProductVat> optionalProductVat = service.get(id);
            if (optionalProductVat.isPresent()) {
                productVatDTO = createProductVatDTO(optionalProductVat.get());
            }
        } else {
            productVatDTO = new ProductVatDTO();
        }
        model.addAttribute("productVat", productVatDTO);
        model.addAttribute("title", "Add Product Vat");


        return "vat/edit";
    }

    @PostMapping("/edit")
    public String processForm(@Valid ProductVatDTO productVatDTO, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message("Validation  Errors :", MessageType.ERROR));
            model.addAttribute("productVat", productVatDTO);
            return "vat/edit";
        }
        try {
            service.save(createBarcode(productVatDTO));
            redirectAttributes.addFlashAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message("Product VAT saved successfully!", MessageType.INFO));
        } catch (ChirembaException e) {
            log.error("Unable to save  product vat", e);
            redirectAttributes.addFlashAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message(e.getMessage(), MessageType.ERROR));
        }
        return "redirect:/product-vat/list/";

    }
    @GetMapping("/list")
    public String productVATList(Model model) {
        final List<ProductVatDTO> barcodes = service.getAll().stream().map(this::createProductVatDTO).collect(Collectors.toList());
        model.addAttribute("list", barcodes);
        return "vat/list";
    }

    private ProductVatDTO createProductVatDTO(ProductVat productVat) {
        return ProductVatDTO.builder().id(productVat.getId())
                .created(productVat.getCreated())
                .version(productVat.getVersion())
                .vatRate(productVat.getVatRate())
                .vatLevel(productVat.getVatLevel())
                .description(productVat.getDescription())
                .build();
    }

    private ProductVat createBarcode(ProductVatDTO productVatDTO) {
        ProductVat productVat = new ProductVat();
        productVat.setDescription(productVatDTO.getDescription());
        productVat.setVatRate(productVatDTO.getVatRate());
        productVat.setVatLevel(productVatDTO.getVatLevel());
        productVat.setVersion(productVatDTO.getVersion());
        productVat.setCreated(productVatDTO.getCreated());
        productVat.setCreatedBy(productVatDTO.getCreatedBy());
        productVat.setModifiedBy(productVatDTO.getModifiedBy());
        return productVat;
    }



}
