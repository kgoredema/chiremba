package com.softhub.user;

import com.softhub.chiremba.business.user.User;
import com.softhub.chiremba.business.user.UserRepository;
import com.softhub.chiremba.business.user.UserService;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;

/**
 * Created by kelvin on 2019/06/05.
 */

@Controller
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final  UserService userService;
    @Autowired
    @Qualifier("dbPasswordEncoder")
    @Lazy
    PasswordEncoder passwordEncoder;


    @GetMapping("/registration")
    public String register(Model model) {
       model.addAttribute("userDTO",new UserDTO()) ;
        return "user/registration";
    }


    @PostMapping("/registration")
    public String submitRegistration(@Valid UserDTO  userDTO,HttpServletRequest request,BindingResult result) {
        if (result.hasErrors()) {
            return "user/registration";
        }
        User user= User.builder().username(userDTO.getUsername()).
                password(passwordEncoder.encode(userDTO.getPassword())).emailAddress(userDTO.getEmail()).build();
        userService.saveUserOnRegistration(user);
        return "redirect:/login";
    }


}
