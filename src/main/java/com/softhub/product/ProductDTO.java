package com.softhub.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by kelvin on 2019/06/13.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {

    private String  code;
    private  String name;
    private  String description;
    private BigDecimal sellingPrice;
    protected LocalDateTime created;
    protected LocalDateTime lastModified;
    protected String createdBy;
    protected String modifiedBy;
    private int version;
}
