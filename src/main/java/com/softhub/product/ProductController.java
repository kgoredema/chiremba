package com.softhub.product;

import com.softhub.cashier.CashierDTO;
import com.softhub.chiremba.business.cashier.Cashier;
import com.softhub.chiremba.business.cashier.CashierService;
import com.softhub.chiremba.business.exception.ChirembaException;
import com.softhub.chiremba.business.product.Product;
import com.softhub.chiremba.business.product.ProductService;
import com.softhub.utils.Message;
import com.softhub.utils.MessageType;
import com.softhub.utils.ModelAttributeConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by kelvin on 2019/06/13.
 */


@Controller
@RequestMapping("/product")
@RequiredArgsConstructor
@Slf4j
public class ProductController {

    private final ProductService service;

    @GetMapping("/edit")
    public String form(@RequestParam(value = "code", required = false) String code, Model model) {
        ProductDTO productDTO = null;
        if (code != null) {
            final Optional<Product> optionalProduct = service.get(code);
            if (optionalProduct.isPresent()) {
                productDTO = createProductDTO(optionalProduct.get());
            }
        } else {
            productDTO = new ProductDTO();
        }
        model.addAttribute("product", productDTO);
        model.addAttribute("title", "Add Product");

        return "product/edit";
    }

    @PostMapping("/edit")
    public String processForm(@Valid ProductDTO productDTO, BindingResult result,Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message("Validation  Errors :", MessageType.ERROR));
            model.addAttribute("product", productDTO);
            return "product/edit";
        }
        try {
            service.save(createProduct(productDTO));
            redirectAttributes.addFlashAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message("Cashier saved successfully!", MessageType.INFO));
        } catch (ChirembaException e) {
            log.error("Unable to save  cashier", e);
            redirectAttributes.addFlashAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message(e.getMessage(), MessageType.ERROR));
        }
        return "redirect:/product/list/";

    }
    @GetMapping("/list")
    public String productList(Model model) {
        final List<ProductDTO> products = service.getAll().stream().map(this::createProductDTO).collect(Collectors.toList());
        model.addAttribute("list", products);
        return "product/list";
    }

    private ProductDTO createProductDTO(Product product) {
        return ProductDTO.builder().code(product.getCode())
                .name(product.getName())
                .created(product.getCreated())
                .version(product.getVersion())
                .description(product.getDescription())
                .sellingPrice(product.getSellingPrice())
                .build();
    }

    private Product createProduct(ProductDTO productDTO) {
        Product product = new Product();
        product.setCode(productDTO.getCode());
        product.setName(productDTO.getName());
        product.setVersion(productDTO.getVersion());
        product.setSellingPrice(productDTO.getSellingPrice());
        product.setDescription(productDTO.getDescription());
        product.setCreated(productDTO.getCreated());
        product.setCreatedBy(productDTO.getCreatedBy());
        product.setModifiedBy(productDTO.getModifiedBy());
        return product;
    }


}
