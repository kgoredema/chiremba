package com.softhub.config;


import com.softhub.chiremba.business.user.User;
import com.softhub.chiremba.business.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import javax.inject.Named;
import java.util.Optional;

@Named(value = SecurityConfiguration.AUDITOR_AWARE_BEAN)
public class AuditorAwareImpl implements AuditorAware<String> {

    @Autowired
    private UserService userService;

    @Override
    public Optional<String> getCurrentAuditor() {
       // final Optional<User> userOptional = userService.findCurrentUser();
        //return userOptional.isPresent() ? Optional.ofNullable(userOptional.get().getUsername()) : Optional.ofNullable("System");
        return Optional.ofNullable("System");
    }
}
