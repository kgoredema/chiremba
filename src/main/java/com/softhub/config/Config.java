package com.softhub.config;

import com.cronutils.model.definition.CronDefinition;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

import static com.cronutils.model.CronType.SPRING;

@Configuration
@EnableJpaRepositories("com.softhub.chiremba")
@EnableTransactionManagement
@EnableJpaAuditing(modifyOnCreate = false, auditorAwareRef = SecurityConfiguration.AUDITOR_AWARE_BEAN)
public class Config {
    private static final String PRIMARY_DATASOURCE = "primaryDatasource";


    @Bean(name = PRIMARY_DATASOURCE)
    @Primary
    public DataSource getDataSource(Environment environment) {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(environment.getProperty("spring.datasource.jdbc.driverClassName"));
        dataSourceBuilder.url(environment.getProperty("spring.datasource.url"));
        dataSourceBuilder.username(environment.getProperty("spring.datasource.username"));
        dataSourceBuilder.password(environment.getProperty("spring.datasource.password"));
        return dataSourceBuilder.build();
    }

    @Bean
    public CronParser cronParser(CronDefinition cronDefinition) {
        return new CronParser(cronDefinition);
    }

    @Bean
    public CronDefinition cronDefinition() {
        return CronDefinitionBuilder.instanceDefinitionFor(SPRING);
    }

    @Bean
    public DefaultListableBeanFactory defaultListableBeanFactory() {
        return new DefaultListableBeanFactory();
    }


}
