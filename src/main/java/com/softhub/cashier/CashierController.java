package com.softhub.cashier;

import com.softhub.chiremba.business.cashier.Cashier;
import com.softhub.chiremba.business.cashier.CashierService;
import com.softhub.chiremba.business.exception.ChirembaException;
import com.softhub.utils.Message;
import com.softhub.utils.MessageType;
import com.softhub.utils.ModelAttributeConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by kelvin on 2019/06/08.
 */

@Controller
@RequestMapping("/cashier")
@RequiredArgsConstructor
@Slf4j
public class CashierController {

    private final CashierService service;

    @GetMapping("/edit")
    public String form(@RequestParam(value = "id", required = false) Long id, Model model) {
        CashierDTO cashierDTO = null;
        if (id != null) {
            final Optional<Cashier> cashier = service.get(id);
            if (cashier.isPresent()) {
                cashierDTO = createCashierDTO(cashier.get());
            }
        } else {
            cashierDTO = new CashierDTO();
        }
        model.addAttribute("cashier", cashierDTO);
        model.addAttribute("title", "Add Cashier");

        return "cashier/edit";
    }

    @PostMapping("/edit")
    public String processForm(@Valid CashierDTO cashierDTO, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "redirect:/cashier/edit";
        }
        try {
            service.save(createCashier(cashierDTO));
            redirectAttributes.addFlashAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message("Cashier saved successfully!", MessageType.INFO));
        } catch (ChirembaException e) {
            log.error("Unable to save  cashier", e);
            redirectAttributes.addFlashAttribute(ModelAttributeConstants.MESSAGE.getName(), new Message(e.getMessage(), MessageType.ERROR));
        }
        return "redirect:/cashier/list/";

    }

    @GetMapping("/list")
    public String cashierList(Model model) {
        final List<CashierDTO> businessPartnerDTOList = service.getAll().stream().map(this::createCashierDTO).collect(Collectors.toList());
        model.addAttribute("list", businessPartnerDTOList);
        return "cashier/list";
    }

    private CashierDTO createCashierDTO(Cashier cashier) {
        return CashierDTO.builder().id(cashier.getId())
                .name(cashier.getName())
                .created(cashier.getCreated())
                .id(cashier.getId())
                .version(cashier.getVersion())
                .lastname(cashier.getLastname())
                .code(cashier.getCode())
                .build();
    }

    private Cashier createCashier(CashierDTO cashierDTO) {
        Cashier cashier = new Cashier();
        cashier.setId(cashierDTO.getId());
        cashier.setName(cashierDTO.getName());
        cashier.setLastname(cashierDTO.getLastname());
        cashier.setVersion(cashierDTO.getVersion());
        cashier.setCreated(cashierDTO.getCreated());
        cashier.setCreatedBy(cashierDTO.getCreatedBy());
        cashier.setModifiedBy(cashierDTO.getModifiedBy());
        cashier.setCode(cashierDTO.getCode());
        return cashier;
    }
}
