package com.softhub.cashier;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@Builder
public class CashierDTO {
    private Long id;
    private String code;
    private String name;
    private String lastname;
    private String fullName;
    protected LocalDateTime created;
    protected LocalDateTime lastModified;
    protected String createdBy;
    protected String modifiedBy;
    private int version;
}
