package com.softhub.admin;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by kelvin on 2019/06/09.
 */
@Controller
@RequestMapping("/admin")
@RequiredArgsConstructor
@Slf4j
public class AdminController {

    @GetMapping("/home")
    public String home(){
        return "admin/home";
    }


    @GetMapping("/cashier/list")
    public String cashier(){
        return "cashier/list";
    }

    @GetMapping("/product/list")
    public String product(){
        return "product/list";
    }



}
