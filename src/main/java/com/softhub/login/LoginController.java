package com.softhub.login;


import com.softhub.chiremba.business.user.Role;
import com.softhub.chiremba.business.user.User;
import com.softhub.chiremba.business.user.UserRepository;
import com.softhub.chiremba.business.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping
public class LoginController {

    @Autowired
    private UserRepository userService;

    @GetMapping("/")
    public String index() {
        return "redirect:/home";
    }


    @GetMapping("/login")
    public String Login(){
      return "login";

    }

}
