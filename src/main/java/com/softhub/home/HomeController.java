package com.softhub.home;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@Controller

public class HomeController {

    @RequestMapping("/home")
    public String home() {
        return "dashboard";
    }

    @RequestMapping("/home/pos")
    public String posHome() {
        return "dashboard-pos";
    }

    @RequestMapping("/home/accounts")
    public String accountsHome(){
        return "dashboard-accounts";
    }

    @RequestMapping("/home/procurement")
    public String procurementHome(){
        return "dashboard-procurement";
    }


}
